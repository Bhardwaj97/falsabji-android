package com.example.falsabji.model;


import com.google.gson.annotations.SerializedName;

public class LoginCredential {

    @SerializedName("key")
    private String email;
    @SerializedName("password")
    private String password;


    public LoginCredential(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }



    public String getPassword() {
        return password;
    }

}
