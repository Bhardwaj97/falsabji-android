package com.example.falsabji.model;

import com.google.gson.annotations.SerializedName;

public class User {
        @SerializedName("firstName")
        private String first_name;
        @SerializedName("lastName")
        private String last_name;
        @SerializedName("phone")
        private String mobile_no;
        @SerializedName("email")
        private String email_id;
        @SerializedName("password")
        private String password;

    public User(String first_name, String last_name, String mobile_no, String email_id, String password) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.mobile_no = mobile_no;
        this.email_id = email_id;
        this.password = password;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public String getEmail_id() {
        return email_id;
    }

    public String getPassword() {
        return password;
    }
}
