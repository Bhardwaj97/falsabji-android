package com.example.falsabji.authentication;

public interface SignUpView {

    void showHideProgress(boolean isShow);
    void onError(String message);
    void onSuccess(String message);

}
