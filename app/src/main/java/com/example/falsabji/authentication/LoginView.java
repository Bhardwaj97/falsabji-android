package com.example.falsabji.authentication;

public interface LoginView {

    void showHideLoginProgress(boolean isShow);
    void onLoginError(String message);
    void onLoginSuccess(String message);
    void onLoginFailure(Throwable t);
}
