package com.example.falsabji.authentication;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.falsabji.R;
import com.example.falsabji.model.User;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SignUpActivity extends AppCompatActivity implements SignUpView, View.OnClickListener {

    private Unbinder unbinder;
    @BindView(R.id.ed_username)
    EditText ed_username;
    @BindView(R.id.ed_mobile)
    EditText ed_mobile_no;
    @BindView(R.id.ed_email)
    EditText ed_email;
    @BindView(R.id.ed_pass)
    EditText ed_password;
    @BindView(R.id.tv_forgot_pass)
    TextView tv_forgot_pass;
    @BindView(R.id.tv_signup_btn)
    TextView tv_signup_btn;
    private SignUpPresenter presenter;
    private User user;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up_layout);
        unbinder = ButterKnife.bind(this);
        progressDialog = new ProgressDialog(this);
        presenter= new SignUpPresenter(this);
        tv_signup_btn.setOnClickListener(this);
    }

    @Override
    public void showHideProgress(boolean isShow) {
        progressDialog.setMessage("please wait..");
        progressDialog.setCancelable(false);
        if(isShow){
            progressDialog.show();
        }else {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onError(String message) {
        System.out.println("error "+message );
        Toast.makeText(this,"error "+message,Toast.LENGTH_LONG).show();

    }

    @Override
    public void onSuccess(String message) {
        Toast.makeText(this,"success "+message,Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this,LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.tv_signup_btn){
            String username = ed_username.getText().toString().trim();
            String names[]  = username.split(" ");
            user = new User(names[0],names[1],ed_mobile_no.getText().toString().trim(),ed_email.getText().toString().trim(),ed_password.getText().toString().trim());
            presenter.userCreate(user);
        }
    }
}
