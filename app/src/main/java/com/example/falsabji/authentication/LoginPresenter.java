package com.example.falsabji.authentication;

import com.example.falsabji.model.LoginCredential;
import com.example.falsabji.utils.AppUtils;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenter {
    private LoginView loginView;

    public LoginPresenter(LoginView loginView) {
        this.loginView = loginView;
    }

    public void loginUser(LoginCredential credential){
        loginView.showHideLoginProgress(true);
        Call<ResponseBody>loginCall= AppUtils.getApi().doLogin(credential);

        loginCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                loginView.showHideLoginProgress(false);
                try {
                    String responseReceived = response.body().string();
                    System.out.println("success "+responseReceived);
                }catch (Exception e){
                    e.printStackTrace();
                }
                if(response.isSuccessful() && response.body()!=null && response.code()==200){

                    loginView.onLoginSuccess(response.message());
                }else {
                    loginView.onLoginError(response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                loginView.showHideLoginProgress(false);
                loginView.onLoginFailure(t);
            }
        });


//        resposeModelCall.enqueue(new Callback<LoginCredential>() {
//            @Override
//            public void onResponse(Call<LoginCredential> call, Response<LoginCredential> response) {
//                loginView.showHideProgress(false);
//                if(response.isSuccessful() && response.body()!=null && response.code()==200){
//                    loginView.onSuccess(response.message());
//                }else {
//                    loginView.onError(response.errorBody().toString());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<LoginCredential> call, Throwable t) {
//                    loginView.showHideProgress(false);
//                    loginView.onFailure(t);
//            }
//        });
    }
}
