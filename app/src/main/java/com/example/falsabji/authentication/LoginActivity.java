package com.example.falsabji.authentication;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.falsabji.R;
import com.example.falsabji.home.HomeActivity;
import com.example.falsabji.model.LoginCredential;
import com.example.falsabji.utils.Validation;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class LoginActivity extends AppCompatActivity implements LoginView, View.OnClickListener {
    private Unbinder unbinder;
    @BindView(R.id.ed_mobile)
    EditText ed_mobile_no;
    @BindView(R.id.ed_pass)
    EditText ed_pass;
    @BindView(R.id.loginBtn)
    TextView login_btn;
    @BindView(R.id.tv_forgot_pass)
    TextView tv_forgot_btn;
    @BindView(R.id.goto_signup)
    TextView goto_signup;

    private ProgressDialog progressDialog;
    private LoginPresenter loginPresenter;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);
        unbinder = ButterKnife.bind(this);
        context = LoginActivity.this;
        progressDialog = new ProgressDialog(this);
        loginPresenter = new LoginPresenter(this);

        login_btn.setOnClickListener(this);
        goto_signup.setOnClickListener(this);

    }


    @Override
    public void showHideLoginProgress(boolean isShow) {
        progressDialog.setMessage("login please wait...");
        progressDialog.setCancelable(false);
        if(isShow){
            progressDialog.show();
        }else {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onLoginError(String message) {
        Toast.makeText(this,"error "+message,Toast.LENGTH_LONG).show();
    }

    @Override
    public void onLoginSuccess(String message) {
        if(message.equalsIgnoreCase("ok")){
        Intent intent = new Intent(context, HomeActivity.class);
        startActivity(intent);
        }

    }

    @Override
    public void onLoginFailure(Throwable t) {

    }

    @Override
    public void onClick(View v) {
            switch (v.getId()){
                case R.id.goto_signup:
                    Intent intent = new Intent(this,SignUpActivity.class);
                    startActivity(intent);
                    break;
                case R.id.loginBtn:
                    setValues();
                    break;
                case R.id.tv_forgot_pass:
                    break;
            }
    }

    private void setValues(){
        String username = ed_mobile_no.getText().toString();
        String password = ed_pass.getText().toString();
        if(Validation.isValidEmail(username) && !password.isEmpty()){
            LoginCredential resposeModel = new LoginCredential(username,password);
            loginPresenter.loginUser(resposeModel);
        }else {
            Toast.makeText(this,"error "+"please check username password!",Toast.LENGTH_LONG).show();
        }
    }
}
