package com.example.falsabji.authentication;

import com.example.falsabji.model.User;
import com.example.falsabji.utils.AppUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpPresenter {
    private SignUpView view;

    public SignUpPresenter(SignUpView view) {
        this.view = view;
    }

    public void userCreate(User user) {
        view.showHideProgress(true);
        Call<User> userCall = AppUtils.getApi().createUser(user);
        userCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                view.showHideProgress(false);
                User responseUser = response.body();
                if (response.isSuccessful() && response.code() == 200 && responseUser != null) {
                    view.onSuccess(response.message());
                } else {
                    view.onError(response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                view.showHideProgress(false);
                view.onError(t.getMessage());
            }
        });

//               .createUser(user, new Callback<User>() {
//            @Override
//            public void onResponse(Call<User> call, Response<User> response) {
//                view.showHideProgress(false);
//                User responseUser = response.body();
//                if(response.isSuccessful() && response.code() == 200 && responseUser!=null ){
//                    view.onSuccess(response.message());
//                }else {
//                     view.onError(response.errorBody().toString());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<User> call, Throwable t) {
//                view.showHideProgress(false);
//                view.onError(t.getMessage());
//            }
//        });

    }
}
