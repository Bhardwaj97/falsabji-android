package com.example.falsabji.api;

import com.example.falsabji.model.LoginCredential;
import com.example.falsabji.model.User;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiService {

    @POST("user/register")
    Call<User> createUser(@Body User user);

    @POST("user/login")
    Call<ResponseBody> doLogin(@Body LoginCredential credential);

}
