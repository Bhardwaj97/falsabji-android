package com.example.falsabji.api;

import com.example.falsabji.model.User;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiManager {

    private static final String BASE_URl="http://www.imsmarttoken.tk:8800/api/";
    private static  Retrofit retrofit=null;
    private static OkHttpClient okHttpClient;
    private static int REQUEST_TIMEOUT = 30;

    public static Retrofit getRetrofit() {

        if (okHttpClient == null)
            initOkHttp();

        if(retrofit ==null){
            retrofit =new Retrofit.Builder()
                    .baseUrl(BASE_URl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return retrofit;
    }

    private static void initOkHttp() {
        OkHttpClient.Builder httpClient = new OkHttpClient().newBuilder()
                .connectTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        httpClient.addInterceptor(interceptor);

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder requestBuilder = original.newBuilder()
                        .addHeader("Accept", "application/json")
                        .addHeader("Content-Type", "application/json");

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        okHttpClient = httpClient.build();
    }
//    private static OkHttpClient provideOkHttp(){
//        return new OkHttpClient.Builder()
//                .connectTimeout(30L, TimeUnit.SECONDS)
//                .writeTimeout(30L,TimeUnit.SECONDS)
//                .readTimeout(30L,TimeUnit.SECONDS)
//                .addInterceptor(provideloggingIntercepter())
//                .retryOnConnectionFailure(false)
//                .cache(null)
//                .build();
//    }
//    private static Interceptor provideloggingIntercepter(){
//        return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
//    }

}
