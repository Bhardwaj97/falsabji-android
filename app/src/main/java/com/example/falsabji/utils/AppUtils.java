package com.example.falsabji.utils;

import android.content.Context;

import androidx.appcompat.app.AlertDialog;

import com.example.falsabji.api.ApiManager;
import com.example.falsabji.api.ApiService;

public class AppUtils {
    public static ApiService getApi() {
        return ApiManager.getRetrofit().create(ApiService.class);
    }

    public static AlertDialog showDialogMessage(Context context, String title, String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).setTitle(title).setMessage(message).show();
        if (alertDialog.isShowing()) {
            alertDialog.cancel();
        }
        return alertDialog;
    }
}
